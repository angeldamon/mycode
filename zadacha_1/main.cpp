#include <iostream>
#include <cstdlib>//для функции rand()
#include <iomanip> // для функции setw()
using namespace std;

void function(int s[], int n)
{
    for (int i = 0; i<n; i++)
    {
        s[i] = rand();
        cout << s[i] << setw(10);
    }
    cout << endl;
}

void main()
{
    /*Объявить два целочисленных массива с разными размерами и написать функцию, которая заполняет их элементы значениями и
    показывает на экран. Функция должна принимать два параметра — массив и его размер.*/
    const int k = 4;
    const int m = 6;
    int a[k] = {};
    int b[m] = {};
    function(a, k);
    function(b, m);
}
