#include <iostream>
#include <cstdlib>//для функции rand()
#include <iomanip> // для функции setw()
using namespace std;

void FullMassive(int a[5][5])
{
    for (int i = 0; i<5; i++)
    {
        for (int j = 0; j<5; j++)
        {
            a[i][j] = (rand() % 3 + 2) * 15;
            cout << setw(10) << a[i][j];
        }
        cout << endl;
    }
}

int max(int a[5][5])
{
    int m = 30;
    for (int i = 0; i<5; i++)
    {
        for (int j = 0; j<5; j++)
        {
            if (a[i][j] >= m)
            {
                m = a[i][j];
            }
        }
    }
    return m;
}

int min(int a[5][5])
{
    int m = 60;
    for (int i = 0; i<5; i++)
    {
        for (int j = 0; j<5; j++)
        {
            if (a[i][j] <= m)
            {
                m = a[i][j];
            }
        }
    }
    return m;
}

int main(int argc, char *argv[])
{
    //Создать двухмерный массив 5 на 5, заполнить его числами от 30 до 60(использовать для этого отдельную функцию)
    //Написать еще две функции, которые ищут максимальный и минимальный элементы массива.
    int a[5][5] = {};
    FullMassive(a);
    cout << endl;
    int m1 = max(a);
    int m2 = min(a);
    cout << m1 << endl;
    cout << m2 << endl;
}
